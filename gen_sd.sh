#!/bin/bash

source common.sh

# Validate usage
if [ "$#" -ne 3 ] || ! [ -d "$1" ] || ! [ -f "$2" ] || ! [ -f "$3" ]; then
  echo "Usage: $0 BootSW_folder Kernel_archive RootFS_archive" >&2
  exit 1
fi

BOOTSW_FOLDER=$1
KERNEL_ARCHIVE=$2
ROOTFS_ARCHIVE=$3

OUTFILE=${OUT_DIR}/sd.img
TMP_MNT=/tmp/riscvee_mnt

# Create output folder
mkdir -p ${OUT_DIR}

# Remove old image if any
rm -f ${OUTFILE}

echo "Creating empty image with partition table..."

# Create disk image suitable for 2GB SD card
truncate -s 800000000 ${OUTFILE}

# Wip all
sgdisk --zap-all ${OUTFILE}

# Create GPT
sgdisk -o ${OUTFILE}

# Create partitions
sgdisk --new 1:81920:+128M --typecode 1:8300 --change-name 1:boot ${OUTFILE}
sgdisk --new 2::0 --typecode 2:8300 --change-name 2:rootfs ${OUTFILE}

echo "Creating empty image with partition table... done"

# Add bootloader(s)
echo "Installing bootloaders..."
dd if=${BOOTSW_FOLDER}/sun20i_d1_spl/nboot/boot0_sdcard_sun20iw1p1.bin of=${OUTFILE} bs=8192 seek=16 conv=notrunc
dd if=${BOOTSW_FOLDER}/u-boot.toc1 of=${OUTFILE} bs=512 seek=32800 conv=notrunc
echo "Installing bootloaders... done"

echo "Formatting partitions..."

# Create per partition loop devices
sudo kpartx -a ${OUTFILE}

# Format partitions
sudo mkfs.ext2 /dev/mapper/loop0p1
sudo mkfs.ext4 /dev/mapper/loop0p2

echo "Formatting partitions... done"

# Populate fs
echo -n "Populating fs... "

# Create temporary mount point
mkdir -p ${TMP_MNT}

# Mount rootfs & boot
sudo mount /dev/mapper/loop0p2 ${TMP_MNT}
sudo tar xfJ ${ROOTFS_ARCHIVE} -C ${TMP_MNT}
sudo mount /dev/mapper/loop0p1 ${TMP_MNT}/boot
sudo tar xfJ ${KERNEL_ARCHIVE} -C ${TMP_MNT}
sudo cp -a ${BOOTSW_FOLDER}/boot.scr ${TMP_MNT}/boot

# Umounting boot & rootfs
sudo umount ${TMP_MNT}/boot
sudo umount ${TMP_MNT}

echo "done"

echo -n "Cleaning up... "

# Remove temporary mount point
rmdir ${TMP_MNT}

# Remove loop devices
sudo kpartx -d ${OUTFILE}

echo "done"
