#!/bin/bash

source common.sh

# Bootstrap chroot environment
if ! [ -d ${BUILD_ROOTFS} ]; then
  # Make sure all parent folders of rootfs folder exist
  mkdir -p ${BUILD_ROOTFS}
  echo "Bootstrap chrrot environment"
  sudo mmdebstrap --architectures=riscv64 --include="dbus,debian-ports-archive-keyring,isc-dhcp-client,openssh-server,popularity-contest,rsyslog,sudo,wpasupplicant" sid ${BUILD_ROOTFS} "deb http://deb.debian.org/debian-ports sid main" "deb http://deb.debian.org/debian-ports unreleased main"
fi

# Apply rootfs patches
mkdir -p ${BUILD_TMP}
cd rootfs_patch
tar cJf ${BUILD_TMP}/rootfs_patch.tar.xz --owner=0 --group=0 .
cd ..
sudo tar xfJ  ${BUILD_TMP}/rootfs_patch.tar.xz -C ${BUILD_ROOTFS}
rm -rf ${BUILD_TMP}

# Configuration inside chrooted environment
sudo cp -a build_rootfs_internal.sh ${BUILD_ROOTFS}/prep.sh
sudo chroot ${BUILD_ROOTFS} /prep.sh
sudo rm ${BUILD_ROOTFS}/prep.sh

# Create rootfs tar.xz
pushd ${BUILD_ROOTFS}
mkdir -p ${OUT_DIR}
sudo tar cJf ${OUT_DIR}/riscvee_rootfs_$(date +%Y%m%d)01.tar.xz .
popd
