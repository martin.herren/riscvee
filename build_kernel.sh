#!/bin/bash

export VERSION=5.17-rc2
export CHECKOUT_VERSION=d1-wip-v5.17-rc2

source common.sh

export ARCH=riscv
export CROSS_COMPILE=riscv64-linux-gnu-

mkdir -p ${BUILD_TMP}/kernel/

# Copy kernel config
mkdir -p ${BUILD_KERNEL}/linux-build/arch/riscv/configs
cp riscvee_defconfig ${BUILD_KERNEL}/linux-build/arch/riscv/configs/riscvee_defconfig

pushd ${BUILD_KERNEL}

# Clone kernel if not existing yet
if ! [ -d linux ]; then
  git clone https://github.com/smaeul/linux.git
fi

cd linux
git checkout ${CHECKOUT_VERSION}
cd ..

# Configure, build kernel and install modules
make -C linux O=${BUILD_KERNEL}/linux-build riscvee_defconfig
make -j $(nproc) -C linux-build V=1

make -j $(nproc) -C linux-build V=1 INSTALL_MOD_PATH=${BUILD_TMP}/kernel/usr modules_install

KVER=$(ls ${BUILD_TMP}/kernel/usr/lib/modules)

# Clone wifi kernel module if not existing yet
if ! [ -d rtl8723ds ]; then
  git clone https://github.com/lwfinger/rtl8723ds.git
fi

# Compile and install wifi driver module
cd rtl8723ds
make -j $(nproc) KSRC=${BUILD_KERNEL}/linux-build modules
mkdir -p ${BUILD_TMP}/kernel/usr/lib/modules/${KVER}/kernel/drivers/net/wireless/
install -D -p -m 644 8723ds.ko ${BUILD_TMP}/kernel/usr/lib/modules/${KVER}/kernel/drivers/net/wireless/8723ds.ko
cd ..

popd

# Cleanup modules
rm ${BUILD_TMP}/kernel/usr/lib/modules/${KVER}/build
rm ${BUILD_TMP}/kernel/usr/lib/modules/${KVER}/source
/usr/sbin/depmod -a -b ${BUILD_TMP}/kernel/usr ${KVER}
mkdir -p ${BUILD_TMP}/kernel/etc
echo "8723ds" >> ${BUILD_TMP}/kernel/etc/modules

# Install kernel
cp -a ${BUILD_KERNEL}/linux-build/arch/riscv/boot ${BUILD_TMP}/kernel/boot

# Create kernel tar.xz
pushd ${BUILD_TMP}/kernel/
mkdir -p ${OUT_DIR}
tar cJf ${OUT_DIR}/riscvee_kernel_${VERSION}_$(date +%Y%m%d)01.tar.xz --owner=0 --group=0 .
popd

# Cleanup tmp
rm -rf ${BUILD_TMP}/kernel/
