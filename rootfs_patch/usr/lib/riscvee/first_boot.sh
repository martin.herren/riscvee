#!/bin/sh

# RiscVee
# Copyright (C) 2022  "Martin Herren" <martin.herren@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# First Boot script. Naive implementation of resizing the rootfs to fill the SD
# This script expects the rootfs to be the 2nd and last partition.
# It will be expanded to the full remaining SD size minus a 1GB swap partition
# that gets created as well

log() {
  >/dev/console printf "[%12.6f] [RiscVee] %s\n" $(cut -f 1 -d ' ' /proc/uptime) "$1"
}

reboot () {
  log "Rebooting now !"

  mount / -o remount,ro
  sync

  echo b > /proc/sysrq-trigger

  sleep 5

  exit 0
}

check_commands() {
  for COMMAND in cut mkswap parted resize2fs sleep; do
    if ! command -v $COMMAND > /dev/null; then
      FAIL_REASON="$COMMAND not found"
      return 1
    fi
  done

  return 0
}

expand_fs() {
  ROOT_DEV=/dev/mmcblk0
  ROOT_PART=1
  ROOT_PART=2
  SWAP_PART=3

  # get partition table and fix gpt for sd size
  log "Getting partition table and fixing GPT"
  PARTITION_TABLE=$(parted -s -f -m ${ROOT_DEV} print | tr -d 's')

  # TODO calculate double ram amount
  log "Resizing rootfs partition"
  parted -s -m -a optimal -- ${ROOT_DEV} resizepart ${ROOT_PART} -1GiB

  log "Creating swap partition"
  parted -s -f -a optimal -- ${ROOT_DEV} mkpart primary linux-swap -1GiB 100%

  log "Resizing rootfs filesystem"
  resize2fs ${ROOT_DEV}p${ROOT_PART}

  log "Formating swap filesystem"
  mkswap ${ROOT_DEV}p${SWAP_PART}

  return 0
}

mount -t proc proc /proc
mount -t sysfs sys /sys
mount -t tmpfs tmp /run
mkdir -p /run/systemd

# Disable first_boot.sh early to prevent looping on it
mount / -o remount,rw
ln -fs /lib/systemd/systemd /usr/sbin/init
sync

echo 1 > /proc/sys/kernel/sysrq

if check_commands; then
  if expand_fs; then
    log "Successfully expanded root filesystem."
  else
    log "Failed expanding filesystem\nError: ${FAIL_REASON}"
  fi
else
  log "Missing dependencies to perform filesystem expansion\nError: ${FAIL_REASON}"
fi

log "Rebooting in 5 seconds..."
sleep 5
reboot
