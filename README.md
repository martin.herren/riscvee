RiscVee
=======

Naive attempt to build a simple bootable SD card image for SiPEED's Lichee RV board.

Currently just wrapping the prebuilt binaries provided by Andreas at https://andreas.welcomes-you.com/boot-sw-debian-risc-v-lichee-rv/ into a script.

Goal would be to build all parts required from source into a minimal, self-expanding SD cart image.

Usage
-----

To launch the generation of the tool, download and extract the bootsw archive and download also the rootfs archive.
Give the path to the extracted bootsw folder and the path to the rootfs archive as arguments:
```
./gen_sd.sh ../andreas/extracted ../andreas/licheerv-debian-rootfs_2022-03-11.tar.xz
```

In return you'll have a out/sd.img file that can be flashed to a SD card and booted.

To log in and set up wifi the original website mentioned above is still the reference.

There will be a warning during boot:
```
GPT:Primary header thinks Alt. header is not at the end of the disk.
```

This is not critical and can be fixed with gdisk, best during resizing the partition to maximize the space on the SD card. For this gdisk must first be installed.
