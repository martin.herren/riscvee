#!/bin/sh

# Set up hostname
echo "riscveee" > /etc/hostname

# Update package information
apt update -y
apt upgrade -y
apt autoremove -y
apt autoclean -y

# Install useful packages
apt install -y parted

# Set default runtlevel to multi-user.target
systemctl set-default multi-user.target

# Create user
useradd -Gsudo -m -s/bin/bash riscvee
echo "riscvee\nriscvee\n" | passwd riscvee

exit
